﻿using AHOY.Entities.Auxiliary;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AHOY.Services.Extensions
{
    public static class DataPagerExtension
    {
        public static async Task<List<TModel>> PaginateAsync<TModel>(this IQueryable<TModel> query, PaginationFilter paginationFilter, CancellationToken cancellationToken = default)
            where TModel : class
        {

            if (paginationFilter is null)
                paginationFilter = new PaginationFilter();
            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            var result =  await query.Skip(skip).Take(paginationFilter.PageSize).ToListAsync();
            return result;            
        }
    }
}
