﻿using AHOY.DataAccessLibrary.MsSqlServer;
using AHOY.Entities;
using AHOY.Entities.Auxiliary;
using AHOY.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHOY.Services.Implementation
{
    public class ReviewService : IReviewService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ILogger<HotelService> _logger;

        public ReviewService(ApplicationDbContext dbContext, ILogger<HotelService> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }


        public async Task<List<Review>> GetReviewsAsync(Guid hotelID, PaginationFilter paginationFilter = null)
        {
            if (paginationFilter is null)
                paginationFilter = new PaginationFilter();
            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            var data = await _dbContext.Reviews
                .AsNoTracking()
                .Where(x => x.HotelID == hotelID)
                .OrderByDescending(x => x.ReviewDate)
                .Skip(skip).Take(paginationFilter.PageSize)
                .ToListAsync();

            return data;
        }
    }
}
