﻿using AHOY.DataAccessLibrary.MsSqlServer;
using AHOY.Entities;
using AHOY.Entities.Auxiliary;
using AHOY.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHOY.Services.Implementation
{
    public class BookingService : IBookingService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ILogger<BookingService> _logger;

        public BookingService(ApplicationDbContext dbContext, ILogger<BookingService> logger)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _logger = logger;
        }


        public async Task<List<Booking>> GetAllBookingsAsync(DateTimeOffset startDate, DateTimeOffset endDate, PaginationFilter paginationFilter = null)
        {
            if (paginationFilter is null)
                paginationFilter = new PaginationFilter();
            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            var data = await _dbContext.Bookings
                .AsNoTracking()
                .Include(x=>x.Hotel)
                .Where(x=> x.CheckInDate>=startDate && x.CheckInDate<=endDate)
                .OrderBy(x => x.CheckInDate)
                .Skip(skip).Take(paginationFilter.PageSize)
                .ToListAsync();

            return data;
        }

        public async Task<Booking> GetBookingAsync(Guid bookingID)
        {
            if (bookingID == Guid.Empty)
                return null;

            return await _dbContext.Bookings
                .Include(x=>x.Hotel)
                .ThenInclude(x=>x.Facilities)
                .FirstOrDefaultAsync(x => x.BookingID == bookingID);
        }

        public async Task<bool> CreateBookingAsync(Booking booking)
        {
            if (booking is null) return false;

            try
            {
                await _dbContext.Bookings.AddAsync(booking);
                var added = await _dbContext.SaveChangesAsync();
                return added>0;
            }
            catch (Exception ex)
            {
                _logger.LogError("Booking creation error", ex);
                return false;
            }
        }

        public async Task BookingEventOccurred(Booking booking, string evt)
        {
            ///
            /// Do any action to handle the event
            /// 

            _logger.LogInformation($"Event occurred for booking id {booking.BookingID}", evt);
            await Task.CompletedTask;
        }
    }
}
