﻿using AHOY.DataAccessLibrary;
using AHOY.DataAccessLibrary.MsSqlServer;
using AHOY.Entities;
using AHOY.Entities.Auxiliary;
using AHOY.Services.Extensions;
using AHOY.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHOY.Services.Implementation
{
    public class HotelService : IHotelService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ILogger<HotelService> _logger;
        public HotelService(ApplicationDbContext dbContext, ILogger<HotelService> logger)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _logger = logger;
        }

        public async Task<List<Hotel>> GetAllHotelsAsync(PaginationFilter paginationFilter = null)
        {
            var data = await _dbContext.Hotels
                .AsNoTracking()
                .IgnoreAutoIncludes()
                .Include(x => x.City)
                .Include(x => x.Facilities)
                .Where(x => x.Active != false)
                .OrderBy(x => x.Name)
                .PaginateAsync(paginationFilter);

            return data;
        }

        public async Task<Hotel> GetHotelAsync(Guid hotelID)
        {
            if (hotelID == Guid.Empty)
                return null;

            return await _dbContext.Hotels.AsNoTracking()
                .IgnoreAutoIncludes()
                .Include(x=>x.Facilities)
                .FirstOrDefaultAsync(x => x.HotelID == hotelID);
        }

        public async Task<List<Hotel>> SearchHotelsAsync(HotelSearchParamFilter searchFilter, PaginationFilter paginationFilter = null)
        {
            if (searchFilter is null)
                return await GetAllHotelsAsync(paginationFilter);

            var data = await _dbContext.Hotels.AsNoTracking()
                .Include(x => x.City)
                .Include(x => x.Facilities)
                .Where(x => String.IsNullOrEmpty(searchFilter.HoteName) ? true : x.Name.ToLower().Contains(searchFilter.HoteName.ToLower()))
                .Where(x => String.IsNullOrEmpty(searchFilter.CityName) ? true : x.City.Name.ToLower().Contains(searchFilter.CityName.ToLower()))
                .Where(x => searchFilter.ClassRating == StarRating.UnRated ? true : x.ClassRating == searchFilter.ClassRating)
                .OrderBy(x => x.Name)
                .PaginateAsync(paginationFilter);

            return data;
        }
    }
}
