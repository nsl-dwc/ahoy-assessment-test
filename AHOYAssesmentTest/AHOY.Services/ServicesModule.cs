﻿using AHOY.DataAccessLibrary.MsSqlServer;
using AHOY.Services.Implementation;
using AHOY.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils.Modules;

namespace AHOY.Services
{
    public class ServicesModule : Module
    {
        public override void Load(IServiceCollection services)
        {
            services.RegisterModule<SqlServerDataAccessModule>(Configuration);
            services.AddScoped<IHotelService, HotelService>();
            services.AddScoped<IBookingService, BookingService>();
            services.AddScoped<IReviewService, ReviewService>();
        }
    }
}
