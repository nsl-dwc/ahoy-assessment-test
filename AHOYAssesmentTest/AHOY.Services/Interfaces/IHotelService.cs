﻿using AHOY.Entities;
using AHOY.Entities.Auxiliary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHOY.Services.Interfaces
{
    public interface IHotelService
    {
        Task<List<Hotel>> GetAllHotelsAsync(PaginationFilter paginationFilter = null);
        Task<Hotel> GetHotelAsync(Guid hotelID);
        Task<List<Hotel>> SearchHotelsAsync(HotelSearchParamFilter searchFilter, PaginationFilter paginationFilter = null);
    }
}
