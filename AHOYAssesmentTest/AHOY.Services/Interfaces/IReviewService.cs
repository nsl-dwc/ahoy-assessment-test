﻿using AHOY.Entities;
using AHOY.Entities.Auxiliary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHOY.Services.Interfaces
{
    public interface IReviewService
    {
        Task<List<Review>> GetReviewsAsync(Guid hotelID, PaginationFilter paginationFilter = null);
    }
}
