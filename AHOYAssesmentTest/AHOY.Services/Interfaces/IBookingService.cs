﻿using AHOY.Entities;
using AHOY.Entities.Auxiliary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHOY.Services.Interfaces
{
    public interface IBookingService
    {
        Task<List<Booking>> GetAllBookingsAsync(DateTimeOffset startDate, DateTimeOffset endDate, PaginationFilter paginationFilter = null);
        Task<Booking> GetBookingAsync(Guid bookingID);
        Task<bool> CreateBookingAsync(Booking booking);
        Task BookingEventOccurred(Booking booking, string evt);

    }
}
