﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AHOY.WebApiApp.Contracts.v1
{
    public static class ApiRoutes
    {
        public const string Root = "api";
        public const string Version = "v1";
        public const string Base = Root + "/" + Version;

        public static class Hotels
        {
            public const string Get = Base + "/hotels/{hotelID}";
            public const string GetAllHotels = Base + "/hotels";
            public const string SearchHotels = Base + "/hotels/search";
            public const string GetReviews = Base + "/hotels/{hotelID}/reviews";
        }

        public static class Booking
        {
            public const string Get = Base + "/bookings/{bookingID}";
            public const string GetAllBookings = Base + "/bookings/{startDate}/{endDate}";
            public const string Create = Base + "/bookings";
        }

    }
}
