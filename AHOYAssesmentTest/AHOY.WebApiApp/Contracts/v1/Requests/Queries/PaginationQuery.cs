﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AHOY.WebApiApp.Contracts.v1.Requests.Queries
{
    public class PaginationQuery
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }

        public PaginationQuery(int pageNumber, int pageSize)
        {
            PageNumber = pageNumber;
            PageSize = pageSize > 10 ? 10 : pageSize;
        }

        public PaginationQuery()
        {
            PageNumber = 1;
            PageSize = 10;
        }

    }
}
