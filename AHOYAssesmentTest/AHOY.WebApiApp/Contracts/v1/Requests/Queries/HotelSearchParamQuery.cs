﻿using AHOY.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AHOY.WebApiApp.Contracts.v1.Requests.Queries
{
    public class HotelSearchParamQuery
    {
        public HotelSearchParamQuery(string hoteName, string cityName, StarRating classRating)
        {
            HoteName = hoteName;
            CityName = cityName;
            ClassRating = classRating;
        }

        public HotelSearchParamQuery()
        {
            HoteName = string.Empty;
            CityName = string.Empty;
            ClassRating = StarRating.UnRated;
        }

        public string  HoteName { get; set; }
        public string CityName { get; set; }
        public StarRating ClassRating { get; set; }
    }
}
