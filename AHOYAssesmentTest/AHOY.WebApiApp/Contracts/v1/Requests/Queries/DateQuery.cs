﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AHOY.WebApiApp.Contracts.v1.Requests.Queries
{
    public class DateQuery
    {
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }

        public DateQuery(DateTimeOffset startDate, DateTimeOffset endDate)
        {
            StartDate = startDate;
            EndDate = endDate;
        }

        public DateQuery()
        {
            StartDate = new DateTime(DateTime.Now.Year,1,1);
            EndDate = new DateTime(DateTime.Now.Year, 12, 31);
        }
    }
}
