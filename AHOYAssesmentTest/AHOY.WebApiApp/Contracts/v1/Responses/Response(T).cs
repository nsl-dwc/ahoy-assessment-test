﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AHOY.WebApiApp.Contracts.v1.Responses
{
    public class Response<T>
    {
        public T data { get; set; }

        public Response(){}
        public Response(T response)
        {
            data =response;
        }
        
        
    }
}
