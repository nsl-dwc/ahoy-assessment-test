﻿using AHOY.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AHOY.WebApiApp.Contracts.v1.Responses
{

    public class HotelResponse
    {
        public Guid HotelID { get; set; }        
        public string Name { get; set; }
        public Guid CityID { get; set; }        
        public City City { get; set; }
       
        public StarRating ClassRating { get; set; }
        public string Address { get; set; }
        public string Location { get; set; } // string type instead of DbGeography is for simplicity
        public string Description { get; set; }

        public decimal PricePerPaxPerNight { get; set; }
        public bool? IsRecomended { get; set; }
        public bool? IsPopular { get; set; } // may be calculated/rated based on bookings quantity

        public string ImageUrl { get; set; }

        public bool Active { get; set; } // Enable/Disabled for listing

        public ICollection<Facility> Facilities { get; set; } = new List<Facility>();
    }
}
