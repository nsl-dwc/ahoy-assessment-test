﻿using AHOY.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AHOY.WebApiApp.Contracts.v1.Responses
{
    public class BookingResponse
    {
        public Guid BookingID { get; set; }
        public Guid HotelID { get; set; }
        public DateTimeOffset CheckInDate { get; set; }
        public DateTimeOffset CheckOutDate { get; set; }
        public int GuestQty { get; set; }
        public decimal PricePerPerson { get; set; }
        public decimal TotalCost { get => PricePerPerson * GuestQty; }
        public Hotel Hotel { get; set; }
    }
}
