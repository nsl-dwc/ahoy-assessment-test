using System.Collections.Generic;

namespace AHOY.Contracts.V1.Responses
{
    public class ErrorResponse
    {
        public ErrorResponse(){}

        public ErrorResponse(ErrorModel error)
        {
            Errors.Add(error);
        }
        
        public List<ErrorModel> Errors { get; } = new List<ErrorModel>();
        
        
    }
}