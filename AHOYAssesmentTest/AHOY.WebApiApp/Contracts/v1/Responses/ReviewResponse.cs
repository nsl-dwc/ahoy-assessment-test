﻿using AHOY.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AHOY.WebApiApp.Contracts.v1.Responses
{
    public class ReviewResponse //: Review
    {
        public Guid ReviewID { get; set; }
        public Guid HotelID { get; set; }
        public string ReviewText { get; set; }
        public int? UserRating { get; set; }
        public DateTimeOffset ReviewDate { get; set; }
    }
}
