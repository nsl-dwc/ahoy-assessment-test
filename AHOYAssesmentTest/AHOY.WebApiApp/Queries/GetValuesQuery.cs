﻿using AHOY.Entities;
using AHOY.Entities.Auxiliary;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AHOY.WebApiApp.Queries
{

    public record GetHotelsQuery(PaginationFilter paginationFilter) : IRequest<List<Hotel>>;
    public record GetHotelQuery(Guid hotelID) : IRequest<Hotel>;
    public record SearchHotelsQuery(HotelSearchParamFilter searchFilter, PaginationFilter paginationFilter) : IRequest<List<Hotel>>;
    public record GetHotelReviewsQuery(Guid hotelID, PaginationFilter paginationFilter) : IRequest<List<Review>>;

    public record GetBookingsQuery(DateTimeOffset startDate, DateTimeOffset endDate, PaginationFilter paginationFilter) : IRequest<List<Booking>>;
    public record GetBookingQuery(Guid bookingID) : IRequest<Booking>;
}
