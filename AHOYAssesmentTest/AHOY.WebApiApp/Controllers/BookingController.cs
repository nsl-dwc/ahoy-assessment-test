﻿using AHOY.Contracts.V1.Responses;
using AHOY.Entities;
using AHOY.Entities.Auxiliary;
using AHOY.Heplers;
using AHOY.Services;
using AHOY.Services.Interfaces;
using AHOY.WebApiApp.Command;
using AHOY.WebApiApp.Contracts.v1;
using AHOY.WebApiApp.Contracts.v1.Requests;
using AHOY.WebApiApp.Contracts.v1.Requests.Queries;
using AHOY.WebApiApp.Contracts.v1.Responses;
using AHOY.WebApiApp.Notifications;
using AHOY.WebApiApp.Queries;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AHOY.WebApiApp.Controllers
{
    public class BookingController: Controller
    {
        //private readonly IBookingService _bookingService;
        //private readonly IHotelService _hotelService;
        private readonly IMapper _mapper;
        private readonly IUriService _uriService;
        private readonly IMediator _mediator;


        public BookingController(IBookingService bookingService, IMapper mapper, IUriService uriService, IHotelService hotelService, IMediator mediator)
        {
            //_bookingService = bookingService;
            _mapper = mapper;
            _uriService = uriService;
            _mediator = mediator;
            //_hotelService = hotelService;
        }

        /// <summary>
        /// Returns a booking by its ID
        /// </summary>        
        /// <param name="bookingID"></param>
        /// <returns>A booking</returns>
        /// <response code="200">Returns the booking by its ID</response>
        /// <response code="404">If nothing found (the booking is null)</response>
        /// <remarks></remarks>
        [HttpGet(ApiRoutes.Booking.Get)]
        [ProducesResponseType(typeof(Response<BookingResponse>), 200)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> GetBooking([FromRoute] Guid bookingID)
        {
            var booking = await _mediator.Send(new GetBookingQuery(bookingID));
            if (booking == null)
                return NotFound("Booking not found");
            var response = _mapper.Map<BookingResponse>(booking);

            return Ok(new Response<BookingResponse>(response));
        }


        /// <summary>
        /// Returns list of the bookings (without booking details)
        /// </summary>
        /// <param name="startDate">yyyy-MM-dd</param>
        /// <param name="endDate">yyyy-MM-dd</param>
        /// <param name="paginationQuery"></param>
        /// <returns>List of the booking</returns>
        /// <response code="200">Returns list of the bookings</response>   
        /// <response code="404">If nothing found</response>
        [HttpGet(ApiRoutes.Booking.GetAllBookings)]
        [ProducesResponseType(typeof(PagedResponse<List<BookingResponse>>), 200)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> GetAllBookings([FromRoute] DateTimeOffset startDate, [FromRoute] DateTimeOffset  endDate,  
            [FromQuery] PaginationQuery paginationQuery = null)
        {
            var paginationFilter = _mapper.Map<PaginationFilter>(paginationQuery);
            var dateFilter = _mapper.Map<DateFilter>(new DateQuery(startDate, endDate));

            var bookings = await _mediator.Send(new GetBookingsQuery(startDate, endDate, paginationFilter));
            if (bookings.Count == 0)
                return NotFound("Bookings not found");

            var response = _mapper.Map<List<BookingResponse>>(bookings);

            if (paginationFilter == null || paginationFilter.PageNumber < 1 || paginationFilter.PageSize < 1)
            {
                return Ok(new PagedResponse<BookingResponse>(response));
            }

            var paginationResponse = PaginationHelpers.CreateDatedPaginatedResponse(_uriService, ApiRoutes.Booking.GetAllBookings, dateFilter, paginationFilter, response);

            return Ok(paginationResponse);
        }


        /// <summary>
        /// Creates hotel booking
        /// </summary>
        /// <returns>ActionResultResponse type</returns>
        /// <param name="request"></param>
        ///<response code="201">Booking successfully created</response>  
        ///<response code="400">Unable to create a booking due to some errors</response>  
        [HttpPost(ApiRoutes.Booking.Create)]
        [ProducesResponseType(typeof(BookingResponse), 201)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<ActionResult> Create([FromBody] CreateBookingRequest request)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var hotel = await _mediator.Send(new GetHotelQuery(request.HotelID));
            if(hotel is null)
                return  BadRequest(new ErrorResponse(new ErrorModel { Message = "Can not find a hotel" }));
            
            var booking = new Booking
            {
                BookingID = Guid.NewGuid(),
                HotelID = request.HotelID,
                CheckInDate = request.CheckInDate,
                CheckOutDate = request.CheckOutDate,
                GuestQty = request.GuestQty,
                PricePerPerson = hotel.PricePerPaxPerNight
            };

            var result = await _mediator.Send(new CreateBookingCommand(booking));
            if (!result)
            {
                return BadRequest(new ErrorResponse(new ErrorModel { Message = "Unable to create a booking" }));
            }

            //Firing a Notification call here for simplicity
            await _mediator.Publish(new NewBookingCreatedNotification(booking));


            var addedBooking = await _mediator.Send(new GetBookingQuery(booking.BookingID));
            var locationUrl = _uriService.GetBookingUri(addedBooking.BookingID.ToString());
            return Created(locationUrl, _mapper.Map<BookingResponse>(addedBooking));
        }

    }
}
