﻿using AHOY.Entities.Auxiliary;
using AHOY.Heplers;
using AHOY.Services;
using AHOY.Services.Interfaces;
using AHOY.WebApiApp.Contracts.v1;
using AHOY.WebApiApp.Contracts.v1.Requests.Queries;
using AHOY.WebApiApp.Contracts.v1.Responses;
using AHOY.WebApiApp.Queries;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AHOY.WebApiApp.Controllers
{
    public class HotelsController: ControllerBase
    {        
        private readonly IMapper _mapper;
        private readonly IUriService _uriService;
        private readonly IMediator _mediator;

        public HotelsController(IMapper mapper, IUriService uriService, IMediator mediator)
        {
            //_hotelService = hotelService ?? throw new ArgumentNullException(nameof(hotelService));
            _mapper = mapper;
            _uriService = uriService;            
            _mediator = mediator;
        }


        /// <summary>
        /// Returns a hotel by its ID
        /// </summary>        
        /// <param name="hotelID"></param>
        /// <returns>A hotel</returns>
        /// <response code="200">Returns a hotel by its ID</response>
        /// <response code="404">If nothing found (the hotel is null)</response>
        /// <remarks></remarks>
        [HttpGet(ApiRoutes.Hotels.Get)]
        [ProducesResponseType(typeof(Response<HotelResponse>), 200)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> GetHotel([FromRoute] Guid hotelID)
        {
            var hotel = await _mediator.Send(new GetHotelQuery(hotelID));
            if (hotel == null)
                return NotFound("Hotel not found");
            var response = _mapper.Map<HotelResponse>(hotel);

            return Ok(new Response<HotelResponse>(response));
        }


        /// <summary>
        /// Returns list of hotels (without hotel details)
        /// </summary>
        /// <param name="paginationQuery"></param>
        /// <returns>List of hotels</returns>
        /// <response code="200">Returns list of hotels</response>   
        /// <response code="404">If nothing found</response>
        [HttpGet(ApiRoutes.Hotels.GetAllHotels)]
        [ProducesResponseType(typeof(PagedResponse<List<HotelResponse>>), 200)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> GetAllHotels( [FromQuery] PaginationQuery paginationQuery = null)
        {
            var paginationFilter = _mapper.Map<PaginationFilter>(paginationQuery);
                        
            var hotels = await _mediator.Send(new GetHotelsQuery(paginationFilter));
            if (!hotels.Any())
                return NotFound("Hotels not found");

            var response = _mapper.Map<List<HotelResponse>>(hotels);

            if (paginationFilter == null || paginationFilter.PageNumber < 1 || paginationFilter.PageSize < 1)
            {
                return Ok(new PagedResponse<HotelResponse>(response));
            }

            var paginationResponse = PaginationHelpers.CreatePaginatedResponse(_uriService, ApiRoutes.Hotels.GetAllHotels, paginationFilter, response);

            return Ok(paginationResponse);
        }


        /// <summary>
        /// Returns list of hotels (without hotel details)
        /// </summary>
        /// <param name="searchParamQuery"></param>
        /// <param name="paginationQuery"></param>
        /// <returns>List of hotels</returns>
        /// <response code="200">Returns list of hotels</response>   
        /// <response code="404">If nothing found</response>
        [HttpGet(ApiRoutes.Hotels.SearchHotels)]
        [ProducesResponseType(typeof(PagedResponse<List<HotelResponse>>), 200)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> SearchHotels([FromQuery] HotelSearchParamQuery searchParamQuery,  [FromQuery] PaginationQuery paginationQuery = null)
        {
            var paginationFilter = _mapper.Map<PaginationFilter>(paginationQuery);
            var searchParamFilter = _mapper.Map<HotelSearchParamFilter>(searchParamQuery);
            
            var hotels = await _mediator.Send(new SearchHotelsQuery(searchParamFilter, paginationFilter));
            if (hotels.Count == 0)
                return NotFound("Hotels not found");

            var response = _mapper.Map<List<HotelResponse>>(hotels);

            if (paginationFilter == null || paginationFilter.PageNumber < 1 || paginationFilter.PageSize < 1)
            {
                return Ok(new PagedResponse<HotelResponse>(response));
            }

            var paginationResponse = PaginationHelpers.CreatePaginatedResponse(_uriService, ApiRoutes.Hotels.GetAllHotels, paginationFilter, response);

            return Ok(paginationResponse);
        }


        /// <summary>
        /// Returns list of reviews
        /// </summary>
        /// <param name="hotelID"></param>
        /// <param name="paginationQuery"></param>
        /// <returns>List of hotel's reviews</returns>
        /// <response code="200">Returns list of reviews</response>   
        /// <response code="404">If nothing found</response>
        [HttpGet(ApiRoutes.Hotels.GetReviews)]
        [ProducesResponseType(typeof(PagedResponse<List<ReviewResponse>>), 200)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> GetHotelReviews( [FromRoute] Guid hotelID, [FromQuery] PaginationQuery paginationQuery = null)
        {
            var paginationFilter = _mapper.Map<PaginationFilter>(paginationQuery);
            
            var reviews = await _mediator.Send(new GetHotelReviewsQuery(hotelID, paginationFilter));
            if (reviews.Count == 0)
                return NotFound("Reviews not found");

            var response = _mapper.Map<List<ReviewResponse>>(reviews);

            if (paginationFilter == null || paginationFilter.PageNumber < 1 || paginationFilter.PageSize < 1)
            {
                return Ok(new PagedResponse<ReviewResponse>(response));
            }

            var paginationResponse = PaginationHelpers.CreatePaginatedResponse(_uriService, ApiRoutes.Hotels.GetReviews, paginationFilter, response);

            return Ok(paginationResponse);
        }
    }
}
