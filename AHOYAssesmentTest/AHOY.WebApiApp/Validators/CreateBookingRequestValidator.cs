﻿using AHOY.WebApiApp.Contracts.v1.Requests;
using FluentValidation;

namespace AHOY.Validators
{
    public class CreateBookingRequestValidator : AbstractValidator<CreateBookingRequest>
    {
        public CreateBookingRequestValidator()
        {            
            RuleFor(x => x.GuestQty).InclusiveBetween(1, 10);
            RuleFor(x => x.CheckInDate).GreaterThanOrEqualTo(System.DateTimeOffset.Now);
            RuleFor(x => x.CheckOutDate).GreaterThan(x=>x.CheckInDate);
        }
    }
}
