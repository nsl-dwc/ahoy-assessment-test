﻿using AHOY.Entities;
using AHOY.Services.Interfaces;
using AHOY.WebApiApp.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AHOY.WebApiApp.Handlers
{
    public class GetHotelHandler : IRequestHandler<GetHotelQuery, Hotel>
    {
        private readonly IHotelService _hotelService;

        public GetHotelHandler(IHotelService hotelService)
        {
            _hotelService = hotelService;
        }

        public async Task<Hotel> Handle(GetHotelQuery request, CancellationToken cancellationToken)
        {
            return await _hotelService.GetHotelAsync(request.hotelID);
        }
    }
}
