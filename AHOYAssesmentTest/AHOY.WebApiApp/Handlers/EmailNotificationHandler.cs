﻿using AHOY.Services.Interfaces;
using AHOY.WebApiApp.Notifications;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AHOY.WebApiApp.Handlers
{
    public class EmailNotificationHandler : INotificationHandler<NewBookingCreatedNotification>
    {
        private readonly IBookingService _bookingService;

        public EmailNotificationHandler(IBookingService bookingService)
        {
            _bookingService = bookingService;
        }

        public async Task Handle(NewBookingCreatedNotification notification, CancellationToken cancellationToken)
        {
            await _bookingService.BookingEventOccurred(notification.booking, "New booking created");
            await Task.CompletedTask;
        }
    }
}
