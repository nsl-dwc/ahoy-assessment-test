﻿using AHOY.Entities;
using AHOY.Services.Interfaces;
using AHOY.WebApiApp.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AHOY.WebApiApp.Handlers
{
    public class SearchHotelsHandler : IRequestHandler<SearchHotelsQuery, List<Hotel>>
    {
        private readonly IHotelService _hotelService;

        public SearchHotelsHandler(IHotelService hotelService)
        {
            _hotelService = hotelService;
        }

        public Task<List<Hotel>> Handle(SearchHotelsQuery request, CancellationToken cancellationToken)
        {
            return _hotelService.SearchHotelsAsync(request.searchFilter, request.paginationFilter);
        }
    }
}
