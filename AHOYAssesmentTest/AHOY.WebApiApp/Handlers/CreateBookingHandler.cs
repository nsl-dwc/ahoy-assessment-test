﻿using AHOY.Entities;
using AHOY.Services.Interfaces;
using AHOY.WebApiApp.Command;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AHOY.WebApiApp.Handlers
{
    public class CreateBookingHandler : IRequestHandler<CreateBookingCommand, bool>
    {
        private readonly IBookingService _bookingService;

        public CreateBookingHandler(IBookingService bookingService)
        {
            _bookingService = bookingService;
        }

        public async Task<bool> Handle(CreateBookingCommand request, CancellationToken cancellationToken)
        {            
            return await _bookingService.CreateBookingAsync(request.booking);                        
        }
    }
}
