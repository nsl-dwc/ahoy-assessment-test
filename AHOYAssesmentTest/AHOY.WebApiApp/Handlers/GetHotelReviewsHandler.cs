﻿using AHOY.Entities;
using AHOY.Services.Interfaces;
using AHOY.WebApiApp.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AHOY.WebApiApp.Handlers
{
    public class GetHotelReviewsHandler: IRequestHandler<GetHotelReviewsQuery, List<Review>>
    {
        private readonly IReviewService _reviewService;

        public GetHotelReviewsHandler(IReviewService reviewService)
        {
            _reviewService = reviewService;
        }

        public async Task<List<Review>> Handle(GetHotelReviewsQuery request, CancellationToken cancellationToken)
        {
            return await _reviewService.GetReviewsAsync(request.hotelID, request.paginationFilter);
        }
    }
}
