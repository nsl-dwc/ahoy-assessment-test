﻿using AHOY.Entities;
using AHOY.Services.Interfaces;
using AHOY.WebApiApp.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AHOY.WebApiApp.Handlers
{
    public class GetBookingHandler : IRequestHandler<GetBookingQuery, Booking>
    {
        private readonly IBookingService _bookingService;

        public GetBookingHandler(IBookingService bookingService)
        {
            _bookingService = bookingService;
        }

        public async Task<Booking> Handle(GetBookingQuery request, CancellationToken cancellationToken)
        {
            return await _bookingService.GetBookingAsync(request.bookingID);
        }
    }
}
