﻿using AHOY.Entities;
using AHOY.Services.Interfaces;
using AHOY.WebApiApp.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AHOY.WebApiApp.Handlers
{
    public class GetHotelsHandler: IRequestHandler<GetHotelsQuery, List<Hotel>>
    {
        private readonly IHotelService _hotelService;
        public GetHotelsHandler(IHotelService hotelService)
        {
            _hotelService = hotelService;
        }

        public async Task<List<Hotel>> Handle(GetHotelsQuery request, CancellationToken cancellationToken)
        {
            return await _hotelService.GetAllHotelsAsync(request.paginationFilter);
        }

    }
}
