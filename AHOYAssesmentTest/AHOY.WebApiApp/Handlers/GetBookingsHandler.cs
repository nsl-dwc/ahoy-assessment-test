﻿using AHOY.Entities;
using AHOY.Services.Interfaces;
using AHOY.WebApiApp.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AHOY.WebApiApp.Handlers
{
    public class GetBookingsHandler : IRequestHandler<GetBookingsQuery, List<Booking>>
    {
        private readonly IBookingService _bookingService;

        public GetBookingsHandler(IBookingService bookingService)
        {
            _bookingService = bookingService;
        }

        public async Task<List<Booking>> Handle(GetBookingsQuery request, CancellationToken cancellationToken)
        {
            return await _bookingService.GetAllBookingsAsync(request.startDate, request.endDate, request.paginationFilter);
        }
    }
}
