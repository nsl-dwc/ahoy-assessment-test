﻿using AHOY.Entities.Auxiliary;
using AHOY.Services;
using AHOY.WebApiApp.Contracts.v1.Requests.Queries;
using AHOY.WebApiApp.Contracts.v1.Responses;
using System.Collections.Generic;
using System.Linq;

namespace AHOY.Heplers
{
    public class PaginationHelpers
    {
        public static PagedResponse<T> CreatePaginatedResponse<T>(IUriService uriService, string routeTemplate,  PaginationFilter paginationFilter, IEnumerable<T> response)
        {
            var nextPage = (paginationFilter.PageNumber >= 1 && response.Count() >= paginationFilter.PageSize)
                ? uriService.GetAllRecordsUri(
                    routeTemplate,
                    new PaginationQuery(paginationFilter.PageNumber + 1, paginationFilter.PageSize)).ToString()
                : null;

            var previousPage = paginationFilter.PageNumber - 1 >= 1
                ? uriService.GetAllRecordsUri(
                    routeTemplate,
                    new PaginationQuery(paginationFilter.PageNumber - 1, paginationFilter.PageSize)).ToString()
                : null;

            return new PagedResponse<T>()
            {
                Data = response,
                PageNumber = paginationFilter.PageNumber >= 1 ? paginationFilter.PageNumber : (int?)null,
                PageSize = paginationFilter.PageSize >= 1 ? paginationFilter.PageSize : (int?)null,
                NextPage = response.Any() ? nextPage : null,
                PreviousPage = previousPage
            };
        }

        public static PagedResponse<T> CreateDatedPaginatedResponse<T>(IUriService uriService, string routeTemplate, DateFilter dateFilter, PaginationFilter paginationFilter, IEnumerable<T> response)
        {
            var nextPage = (paginationFilter.PageNumber >= 1 && response.Count() >= paginationFilter.PageSize)
                ? uriService.GetAllDatedRecordsUri(
                    routeTemplate,
                    new DateQuery(dateFilter.StartDate, dateFilter.EndDate),
                    new PaginationQuery(paginationFilter.PageNumber + 1, paginationFilter.PageSize)).ToString()
                : null;

            var previousPage = paginationFilter.PageNumber - 1 >= 1
                ? uriService.GetAllDatedRecordsUri(
                    routeTemplate,
                    new DateQuery(dateFilter.StartDate, dateFilter.EndDate),
                    new PaginationQuery(paginationFilter.PageNumber - 1, paginationFilter.PageSize)).ToString()
                : null;

            return new PagedResponse<T>()
            {
                Data = response,
                PageNumber = paginationFilter.PageNumber >= 1 ? paginationFilter.PageNumber : (int?)null,
                PageSize = paginationFilter.PageSize >= 1 ? paginationFilter.PageSize : (int?)null,
                NextPage = response.Any() ? nextPage : null,
                PreviousPage = previousPage
            };
        }
    }
}
