﻿using AHOY.Entities;
using AHOY.WebApiApp.Contracts.v1.Responses;

namespace AHOY.MappingProfiles
{
    public class ModelToResponseProfile: AutoMapper.Profile
    {
        public ModelToResponseProfile()
        {
            CreateMap<Hotel, HotelResponse>();
            CreateMap<Booking, BookingResponse>();
            CreateMap<Review, ReviewResponse>();
        }
    }
}
