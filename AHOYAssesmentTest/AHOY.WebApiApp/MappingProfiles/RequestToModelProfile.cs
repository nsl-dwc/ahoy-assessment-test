﻿using AHOY.Entities.Auxiliary;
using AHOY.WebApiApp.Contracts.v1.Requests.Queries;
using AutoMapper;

namespace AHOY.MappingProfiles
{
    public class RequestToModelProfile: Profile
    {
        public RequestToModelProfile()
        {
            CreateMap<PaginationQuery, PaginationFilter>();
            CreateMap<DateQuery, DateFilter>();
            CreateMap<HotelSearchParamQuery, HotelSearchParamFilter>();
        }
    }
}
