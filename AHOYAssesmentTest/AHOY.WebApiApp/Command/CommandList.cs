﻿using AHOY.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AHOY.WebApiApp.Command
{
    public record CreateBookingCommand(Booking booking) : IRequest<bool>;
}
