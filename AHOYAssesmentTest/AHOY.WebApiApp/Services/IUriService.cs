﻿using AHOY.WebApiApp.Contracts.v1.Requests.Queries;
using System;

namespace AHOY.Services
{
    public interface IUriService
    {
        Uri GetHotelUri(string orderID);        
        Uri GetAllRecordsUri(string routeTemplate, PaginationQuery pagination = null);
        Uri GetAllDatedRecordsUri(string routeTemplate, DateQuery dateQuery, PaginationQuery pagination = null);

        Uri GetBookingUri(string bookingID);
    }
}
