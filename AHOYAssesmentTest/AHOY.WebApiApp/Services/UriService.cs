﻿using AHOY.Services;
using AHOY.WebApiApp.Contracts.v1;
using AHOY.WebApiApp.Contracts.v1.Requests.Queries;
using Microsoft.AspNetCore.WebUtilities;
using System;


namespace AHOY.Services
{
    public class UriService : IUriService
    {
        private readonly string _baseUri;
        public UriService(string baseUri)
        {
            _baseUri = baseUri;
        }

     
        public Uri GetAllRecordsUri(string routeTemplate,PaginationQuery pagination = null)
        {
            var uriStr = _baseUri + routeTemplate;
            var uri = new Uri(uriStr);

            if (pagination == null)
            {
                return uri;
            }
            var modifiedUri = QueryHelpers.AddQueryString(uriStr, "pageNumber", pagination.PageNumber.ToString());
            modifiedUri = QueryHelpers.AddQueryString(modifiedUri, "pageSize", pagination.PageSize.ToString());

            return new Uri(modifiedUri);
        }

        public Uri GetAllDatedRecordsUri(string routeTemplate, DateQuery dateQuery, PaginationQuery pagination = null)
        {
            if (dateQuery == null)
                dateQuery = new DateQuery();

            var uriStr = _baseUri + routeTemplate;
            uriStr = uriStr.Replace("{startDate}", dateQuery.StartDate.ToString("yyyy-MM-dd"));
            uriStr = uriStr.Replace("{endDate}", dateQuery.EndDate.ToString("yyyy-MM-dd"));

            var uri = new Uri(uriStr);

            if (pagination == null)
            {
                return uri;
            }
            var modifiedUri = QueryHelpers.AddQueryString(uriStr, "pageNumber", pagination.PageNumber.ToString());
            modifiedUri = QueryHelpers.AddQueryString(modifiedUri, "pageSize", pagination.PageSize.ToString());

            return new Uri(modifiedUri);
        }

        public Uri GetHotelUri(string hotelID)
        {
            return new Uri(_baseUri + ApiRoutes.Hotels.Get.Replace("{hotelID}", hotelID));
        }

        public Uri GetBookingUri(string bookingID)
        {
            return new Uri(_baseUri + ApiRoutes.Booking.Get.Replace("{bookingID}", bookingID));
        }
    }
}
