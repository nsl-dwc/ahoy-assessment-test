﻿using AHOY.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AHOY.DataAccessLibrary
{
    public interface IDBContext
    {

        DbSet<Hotel> Hotels { get; set; }
        DbSet<Review> Reviews { get; set; }
        DbSet<Facility> Facilities { get; set; }
        DbSet<Booking> Bookings { get; set; }
        DbSet<City> Cities { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
        int SaveChanges();
    }
}
