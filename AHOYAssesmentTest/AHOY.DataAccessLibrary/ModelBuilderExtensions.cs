﻿using AHOY.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHOY.DataAccessLibrary
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            List<City> cityList = new List<City>() {
                new City{ CityID = Guid.NewGuid(), Name = "City 1"},
                new City{ CityID = Guid.NewGuid(), Name = "City 2"},
                new City{ CityID = Guid.NewGuid(), Name = "City 3"}
            };

            List<Facility> facList = new List<Facility>() {
                new Facility { FacilityID = Guid.NewGuid(), FacilityName = "Breakfast" },
                new Facility { FacilityID = Guid.NewGuid(), FacilityName = "WiFi" },
                new Facility { FacilityID = Guid.NewGuid(), FacilityName = "Parking" },
                new Facility { FacilityID = Guid.NewGuid(), FacilityName = "SPA" }
            };

            List<Hotel> hotelList = new List<Hotel>() {
                new Hotel { HotelID = Guid.NewGuid(), Name = "Hotel # 1", CityID = cityList[0].CityID, ClassRating = StarRating.One, Address = "Address of Hotel 1", Description = "Description of Hotel 1", Location = "", IsPopular = null, IsRecomended = null, PricePerPaxPerNight = 101.00M, ImageUrl = "hotel_image.pic", Active = true },
                new Hotel { HotelID = Guid.NewGuid(), Name = "Hotel # 2", CityID = cityList[0].CityID, ClassRating = StarRating.Two, Address = "Address of Hotel 2", Description = "Description of Hotel 2", Location = "", IsPopular = false, IsRecomended = false, PricePerPaxPerNight = 102.00M, ImageUrl = "hotel_image.pic", Active = true },
                new Hotel { HotelID = Guid.NewGuid(), Name = "Hotel # 3", CityID = cityList[1].CityID, ClassRating = StarRating.Three, Address = "Address of Hotel 3", Description = "Description of Hotel 3", Location = "", IsPopular = true, IsRecomended = false, PricePerPaxPerNight = 103.00M, ImageUrl = "hotel_image.pic", Active = true },
                new Hotel { HotelID = Guid.NewGuid(), Name = "Hotel # 4", CityID = cityList[1].CityID, ClassRating = StarRating.Four, Address = "Address of Hotel 4", Description = "Description of Hotel 4", Location = "", IsPopular = true, IsRecomended = true, PricePerPaxPerNight = 104.00M, ImageUrl = "hotel_image.pic", Active = true },
                new Hotel { HotelID = Guid.NewGuid(), Name = "Hotel # 5", CityID = cityList[2].CityID, ClassRating = StarRating.Five, Address = "Address of Hotel 5", Description = "Description of Hotel 5", Location = "", IsPopular = true, IsRecomended = true, PricePerPaxPerNight = 105.00M, ImageUrl = "hotel_image.pic", Active = true }
            };

            List<Review> revList = new List<Review>() {
                new Review { ReviewID = Guid.NewGuid(), HotelID = hotelList[0].HotelID, ReviewText = "Hotel 1 Review text 1", UserRating = 1, ReviewDate = DateTimeOffset.Now.AddMinutes(-1440) },
                new Review { ReviewID = Guid.NewGuid(), HotelID = hotelList[0].HotelID, ReviewText = "Hotel 1 Review text 2", UserRating = 2, ReviewDate = DateTimeOffset.Now.AddMinutes(-1440*2) },
                new Review { ReviewID = Guid.NewGuid(), HotelID = hotelList[1].HotelID, ReviewText = "Hotel 2 Review text 1", UserRating = 5, ReviewDate = DateTimeOffset.Now.AddMinutes(-1440*3) }
            };

            modelBuilder.Entity<Facility>(f =>
            {
                f.HasData(new[] { facList[0], facList[1], facList[2], facList[3] });
            });
            modelBuilder.Entity<City>(c =>
            {
                c.HasData(cityList);
            });

            modelBuilder.Entity<Hotel>(h =>
            {
                h.HasData(new[] { hotelList[0], hotelList[1], hotelList[2], hotelList[3], hotelList[4], });            
            });

            //modelBuilder.Entity<HotelFacility>(hf =>
            //{
            //    hf.HasData(hfList[0], hfList[1], hfList[2], hfList[3], hfList[4], hfList[5] );
            //});

            modelBuilder.Entity<Review>(hf =>
            {
                hf.HasData(revList[0], revList[1], revList[2]);
            });

            modelBuilder.Entity<Hotel>()
                .HasMany(h => h.Facilities)
                .WithMany(f => f.Hotels)
                .UsingEntity(j => j.HasData(
                    new { HotelsHotelID = hotelList[0].HotelID, FacilitiesFacilityID = facList[0].FacilityID },
                    new { HotelsHotelID = hotelList[0].HotelID, FacilitiesFacilityID = facList[1].FacilityID },
                    new { HotelsHotelID = hotelList[0].HotelID, FacilitiesFacilityID = facList[2].FacilityID },
                    new { HotelsHotelID = hotelList[1].HotelID, FacilitiesFacilityID = facList[0].FacilityID },
                    new { HotelsHotelID = hotelList[1].HotelID, FacilitiesFacilityID = facList[3].FacilityID },
                    new { HotelsHotelID = hotelList[2].HotelID, FacilitiesFacilityID = facList[3].FacilityID }
                ));


        }
    }
}
