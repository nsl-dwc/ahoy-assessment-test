﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AHOY.DataAccessLibrary.Migrations
{
    public partial class BokingTotalPriceAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "HotelFacilites",
                keyColumns: new[] { "FacilitiesFacilityID", "HotelsHotelID" },
                keyValues: new object[] { new Guid("5cc965c9-24ed-44bb-b11b-865fee07ca42"), new Guid("1c83de98-bed7-460e-a6bc-cb8f98f570da") });

            migrationBuilder.DeleteData(
                table: "HotelFacilites",
                keyColumns: new[] { "FacilitiesFacilityID", "HotelsHotelID" },
                keyValues: new object[] { new Guid("8db20b41-dbe3-44b9-b347-b28c990a56a3"), new Guid("1c83de98-bed7-460e-a6bc-cb8f98f570da") });

            migrationBuilder.DeleteData(
                table: "HotelFacilites",
                keyColumns: new[] { "FacilitiesFacilityID", "HotelsHotelID" },
                keyValues: new object[] { new Guid("8db20b41-dbe3-44b9-b347-b28c990a56a3"), new Guid("c29d678e-4985-468f-a525-ebb88fc150e0") });

            migrationBuilder.DeleteData(
                table: "HotelFacilites",
                keyColumns: new[] { "FacilitiesFacilityID", "HotelsHotelID" },
                keyValues: new object[] { new Guid("b0bb0b79-1922-40eb-89a3-1c0e7737663b"), new Guid("b30152f7-25b2-4ac1-9ccf-31d401d876f3") });

            migrationBuilder.DeleteData(
                table: "HotelFacilites",
                keyColumns: new[] { "FacilitiesFacilityID", "HotelsHotelID" },
                keyValues: new object[] { new Guid("b0bb0b79-1922-40eb-89a3-1c0e7737663b"), new Guid("c29d678e-4985-468f-a525-ebb88fc150e0") });

            migrationBuilder.DeleteData(
                table: "HotelFacilites",
                keyColumns: new[] { "FacilitiesFacilityID", "HotelsHotelID" },
                keyValues: new object[] { new Guid("b3d1b6b1-999d-4157-bfcb-0e5f8fc45c9c"), new Guid("1c83de98-bed7-460e-a6bc-cb8f98f570da") });

            migrationBuilder.DeleteData(
                table: "Hotels",
                keyColumn: "HotelID",
                keyValue: new Guid("2ad4c38b-bd8e-4d62-808c-bee7bad884d5"));

            migrationBuilder.DeleteData(
                table: "Hotels",
                keyColumn: "HotelID",
                keyValue: new Guid("2f3a1a09-69ee-488f-880e-fd1d38e28027"));

            migrationBuilder.DeleteData(
                table: "Reviews",
                keyColumn: "ReviewID",
                keyValue: new Guid("5dcb3b09-ebd9-4f54-8f3a-5c323331061e"));

            migrationBuilder.DeleteData(
                table: "Reviews",
                keyColumn: "ReviewID",
                keyValue: new Guid("a687dc49-e155-427b-aba0-ca4193c68634"));

            migrationBuilder.DeleteData(
                table: "Reviews",
                keyColumn: "ReviewID",
                keyValue: new Guid("deedb09c-34ef-42f1-b3d1-dec659cb2157"));

            migrationBuilder.DeleteData(
                table: "Cities",
                keyColumn: "CityID",
                keyValue: new Guid("7268be33-9c13-4773-b409-eb7d064291a8"));

            migrationBuilder.DeleteData(
                table: "Facilities",
                keyColumn: "FacilityID",
                keyValue: new Guid("5cc965c9-24ed-44bb-b11b-865fee07ca42"));

            migrationBuilder.DeleteData(
                table: "Facilities",
                keyColumn: "FacilityID",
                keyValue: new Guid("8db20b41-dbe3-44b9-b347-b28c990a56a3"));

            migrationBuilder.DeleteData(
                table: "Facilities",
                keyColumn: "FacilityID",
                keyValue: new Guid("b0bb0b79-1922-40eb-89a3-1c0e7737663b"));

            migrationBuilder.DeleteData(
                table: "Facilities",
                keyColumn: "FacilityID",
                keyValue: new Guid("b3d1b6b1-999d-4157-bfcb-0e5f8fc45c9c"));

            migrationBuilder.DeleteData(
                table: "Hotels",
                keyColumn: "HotelID",
                keyValue: new Guid("1c83de98-bed7-460e-a6bc-cb8f98f570da"));

            migrationBuilder.DeleteData(
                table: "Hotels",
                keyColumn: "HotelID",
                keyValue: new Guid("b30152f7-25b2-4ac1-9ccf-31d401d876f3"));

            migrationBuilder.DeleteData(
                table: "Hotels",
                keyColumn: "HotelID",
                keyValue: new Guid("c29d678e-4985-468f-a525-ebb88fc150e0"));

            migrationBuilder.DeleteData(
                table: "Cities",
                keyColumn: "CityID",
                keyValue: new Guid("202c5c6e-4cb6-4f4b-8c31-3e3ce69eb872"));

            migrationBuilder.DeleteData(
                table: "Cities",
                keyColumn: "CityID",
                keyValue: new Guid("b81fb318-a4e3-4119-90d2-40ea62657df0"));

            migrationBuilder.AddColumn<decimal>(
                name: "PricePerPerson",
                table: "Bookings",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.InsertData(
                table: "Cities",
                columns: new[] { "CityID", "Name" },
                values: new object[,]
                {
                    { new Guid("38a1c727-bbe1-4519-8648-cbe334aec4f9"), "City 1" },
                    { new Guid("f7985501-9eb0-4b31-8d46-9b2eb81d2a5a"), "City 2" },
                    { new Guid("d610d724-9bba-4785-8d6f-4d6bb5609175"), "City 3" }
                });

            migrationBuilder.InsertData(
                table: "Facilities",
                columns: new[] { "FacilityID", "FacilityName" },
                values: new object[,]
                {
                    { new Guid("35816bfb-0d3a-48be-9337-bf87eb29f4d7"), "Breakfast" },
                    { new Guid("3ea5adfe-bbd2-42a8-9ad1-1b8d5ae3b9ba"), "WiFi" },
                    { new Guid("376bd27a-f573-4ebc-9af7-e9ce919e6c00"), "Parking" },
                    { new Guid("fa42ac14-9791-42c8-9a43-c831150b86c0"), "SPA" }
                });

            migrationBuilder.InsertData(
                table: "Reviews",
                columns: new[] { "ReviewID", "HotelID", "ReviewDate", "ReviewText", "UserRating" },
                values: new object[,]
                {
                    { new Guid("15e6958c-ad02-4571-a7b9-5f9808b41517"), new Guid("7013309c-9e82-445d-bea6-d67c0a11ae96"), new DateTimeOffset(new DateTime(2021, 10, 2, 22, 9, 22, 265, DateTimeKind.Unspecified).AddTicks(7777), new TimeSpan(0, 4, 0, 0, 0)), "Hotel 1 Review text 1", 1 },
                    { new Guid("45ba461f-0512-4f1b-9d77-e646c691ae64"), new Guid("7013309c-9e82-445d-bea6-d67c0a11ae96"), new DateTimeOffset(new DateTime(2021, 10, 1, 22, 9, 22, 269, DateTimeKind.Unspecified).AddTicks(6122), new TimeSpan(0, 4, 0, 0, 0)), "Hotel 1 Review text 2", 2 },
                    { new Guid("0b65a019-2b6c-4194-9315-93b873d4ab05"), new Guid("9db6ba15-71f6-4c48-8fe8-bf9acc3cab5b"), new DateTimeOffset(new DateTime(2021, 9, 30, 22, 9, 22, 269, DateTimeKind.Unspecified).AddTicks(6181), new TimeSpan(0, 4, 0, 0, 0)), "Hotel 2 Review text 1", 5 }
                });

            migrationBuilder.InsertData(
                table: "Hotels",
                columns: new[] { "HotelID", "Active", "Address", "CityID", "ClassRating", "Description", "ImageUrl", "IsPopular", "IsRecomended", "Location", "Name", "PricePerPaxPerNight" },
                values: new object[,]
                {
                    { new Guid("7013309c-9e82-445d-bea6-d67c0a11ae96"), true, "Address of Hotel 1", new Guid("38a1c727-bbe1-4519-8648-cbe334aec4f9"), 1, "Description of Hotel 1", "hotel_image.pic", null, null, "", "Hotel # 1", 101.00m },
                    { new Guid("9db6ba15-71f6-4c48-8fe8-bf9acc3cab5b"), true, "Address of Hotel 2", new Guid("38a1c727-bbe1-4519-8648-cbe334aec4f9"), 2, "Description of Hotel 2", "hotel_image.pic", false, false, "", "Hotel # 2", 102.00m },
                    { new Guid("df50c377-1cc6-4c12-a7e4-29a9f210d756"), true, "Address of Hotel 3", new Guid("f7985501-9eb0-4b31-8d46-9b2eb81d2a5a"), 3, "Description of Hotel 3", "hotel_image.pic", true, false, "", "Hotel # 3", 103.00m },
                    { new Guid("6f63958f-d6e0-472c-af97-4f832522419c"), true, "Address of Hotel 4", new Guid("f7985501-9eb0-4b31-8d46-9b2eb81d2a5a"), 4, "Description of Hotel 4", "hotel_image.pic", true, true, "", "Hotel # 4", 104.00m },
                    { new Guid("ff5f5ecf-1204-4cfe-9dfd-2002cc85c21e"), true, "Address of Hotel 5", new Guid("d610d724-9bba-4785-8d6f-4d6bb5609175"), 5, "Description of Hotel 5", "hotel_image.pic", true, true, "", "Hotel # 5", 105.00m }
                });

            migrationBuilder.InsertData(
                table: "HotelFacilites",
                columns: new[] { "FacilitiesFacilityID", "HotelsHotelID" },
                values: new object[,]
                {
                    { new Guid("35816bfb-0d3a-48be-9337-bf87eb29f4d7"), new Guid("7013309c-9e82-445d-bea6-d67c0a11ae96") },
                    { new Guid("3ea5adfe-bbd2-42a8-9ad1-1b8d5ae3b9ba"), new Guid("7013309c-9e82-445d-bea6-d67c0a11ae96") },
                    { new Guid("376bd27a-f573-4ebc-9af7-e9ce919e6c00"), new Guid("7013309c-9e82-445d-bea6-d67c0a11ae96") },
                    { new Guid("35816bfb-0d3a-48be-9337-bf87eb29f4d7"), new Guid("9db6ba15-71f6-4c48-8fe8-bf9acc3cab5b") },
                    { new Guid("fa42ac14-9791-42c8-9a43-c831150b86c0"), new Guid("9db6ba15-71f6-4c48-8fe8-bf9acc3cab5b") },
                    { new Guid("fa42ac14-9791-42c8-9a43-c831150b86c0"), new Guid("df50c377-1cc6-4c12-a7e4-29a9f210d756") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "HotelFacilites",
                keyColumns: new[] { "FacilitiesFacilityID", "HotelsHotelID" },
                keyValues: new object[] { new Guid("35816bfb-0d3a-48be-9337-bf87eb29f4d7"), new Guid("7013309c-9e82-445d-bea6-d67c0a11ae96") });

            migrationBuilder.DeleteData(
                table: "HotelFacilites",
                keyColumns: new[] { "FacilitiesFacilityID", "HotelsHotelID" },
                keyValues: new object[] { new Guid("35816bfb-0d3a-48be-9337-bf87eb29f4d7"), new Guid("9db6ba15-71f6-4c48-8fe8-bf9acc3cab5b") });

            migrationBuilder.DeleteData(
                table: "HotelFacilites",
                keyColumns: new[] { "FacilitiesFacilityID", "HotelsHotelID" },
                keyValues: new object[] { new Guid("376bd27a-f573-4ebc-9af7-e9ce919e6c00"), new Guid("7013309c-9e82-445d-bea6-d67c0a11ae96") });

            migrationBuilder.DeleteData(
                table: "HotelFacilites",
                keyColumns: new[] { "FacilitiesFacilityID", "HotelsHotelID" },
                keyValues: new object[] { new Guid("3ea5adfe-bbd2-42a8-9ad1-1b8d5ae3b9ba"), new Guid("7013309c-9e82-445d-bea6-d67c0a11ae96") });

            migrationBuilder.DeleteData(
                table: "HotelFacilites",
                keyColumns: new[] { "FacilitiesFacilityID", "HotelsHotelID" },
                keyValues: new object[] { new Guid("fa42ac14-9791-42c8-9a43-c831150b86c0"), new Guid("9db6ba15-71f6-4c48-8fe8-bf9acc3cab5b") });

            migrationBuilder.DeleteData(
                table: "HotelFacilites",
                keyColumns: new[] { "FacilitiesFacilityID", "HotelsHotelID" },
                keyValues: new object[] { new Guid("fa42ac14-9791-42c8-9a43-c831150b86c0"), new Guid("df50c377-1cc6-4c12-a7e4-29a9f210d756") });

            migrationBuilder.DeleteData(
                table: "Hotels",
                keyColumn: "HotelID",
                keyValue: new Guid("6f63958f-d6e0-472c-af97-4f832522419c"));

            migrationBuilder.DeleteData(
                table: "Hotels",
                keyColumn: "HotelID",
                keyValue: new Guid("ff5f5ecf-1204-4cfe-9dfd-2002cc85c21e"));

            migrationBuilder.DeleteData(
                table: "Reviews",
                keyColumn: "ReviewID",
                keyValue: new Guid("0b65a019-2b6c-4194-9315-93b873d4ab05"));

            migrationBuilder.DeleteData(
                table: "Reviews",
                keyColumn: "ReviewID",
                keyValue: new Guid("15e6958c-ad02-4571-a7b9-5f9808b41517"));

            migrationBuilder.DeleteData(
                table: "Reviews",
                keyColumn: "ReviewID",
                keyValue: new Guid("45ba461f-0512-4f1b-9d77-e646c691ae64"));

            migrationBuilder.DeleteData(
                table: "Cities",
                keyColumn: "CityID",
                keyValue: new Guid("d610d724-9bba-4785-8d6f-4d6bb5609175"));

            migrationBuilder.DeleteData(
                table: "Facilities",
                keyColumn: "FacilityID",
                keyValue: new Guid("35816bfb-0d3a-48be-9337-bf87eb29f4d7"));

            migrationBuilder.DeleteData(
                table: "Facilities",
                keyColumn: "FacilityID",
                keyValue: new Guid("376bd27a-f573-4ebc-9af7-e9ce919e6c00"));

            migrationBuilder.DeleteData(
                table: "Facilities",
                keyColumn: "FacilityID",
                keyValue: new Guid("3ea5adfe-bbd2-42a8-9ad1-1b8d5ae3b9ba"));

            migrationBuilder.DeleteData(
                table: "Facilities",
                keyColumn: "FacilityID",
                keyValue: new Guid("fa42ac14-9791-42c8-9a43-c831150b86c0"));

            migrationBuilder.DeleteData(
                table: "Hotels",
                keyColumn: "HotelID",
                keyValue: new Guid("7013309c-9e82-445d-bea6-d67c0a11ae96"));

            migrationBuilder.DeleteData(
                table: "Hotels",
                keyColumn: "HotelID",
                keyValue: new Guid("9db6ba15-71f6-4c48-8fe8-bf9acc3cab5b"));

            migrationBuilder.DeleteData(
                table: "Hotels",
                keyColumn: "HotelID",
                keyValue: new Guid("df50c377-1cc6-4c12-a7e4-29a9f210d756"));

            migrationBuilder.DeleteData(
                table: "Cities",
                keyColumn: "CityID",
                keyValue: new Guid("38a1c727-bbe1-4519-8648-cbe334aec4f9"));

            migrationBuilder.DeleteData(
                table: "Cities",
                keyColumn: "CityID",
                keyValue: new Guid("f7985501-9eb0-4b31-8d46-9b2eb81d2a5a"));

            migrationBuilder.DropColumn(
                name: "PricePerPerson",
                table: "Bookings");

            migrationBuilder.InsertData(
                table: "Cities",
                columns: new[] { "CityID", "Name" },
                values: new object[,]
                {
                    { new Guid("b81fb318-a4e3-4119-90d2-40ea62657df0"), "City 1" },
                    { new Guid("202c5c6e-4cb6-4f4b-8c31-3e3ce69eb872"), "City 2" },
                    { new Guid("7268be33-9c13-4773-b409-eb7d064291a8"), "City 3" }
                });

            migrationBuilder.InsertData(
                table: "Facilities",
                columns: new[] { "FacilityID", "FacilityName" },
                values: new object[,]
                {
                    { new Guid("8db20b41-dbe3-44b9-b347-b28c990a56a3"), "Breakfast" },
                    { new Guid("b3d1b6b1-999d-4157-bfcb-0e5f8fc45c9c"), "WiFi" },
                    { new Guid("5cc965c9-24ed-44bb-b11b-865fee07ca42"), "Parking" },
                    { new Guid("b0bb0b79-1922-40eb-89a3-1c0e7737663b"), "SPA" }
                });

            migrationBuilder.InsertData(
                table: "Reviews",
                columns: new[] { "ReviewID", "HotelID", "ReviewDate", "ReviewText", "UserRating" },
                values: new object[,]
                {
                    { new Guid("5dcb3b09-ebd9-4f54-8f3a-5c323331061e"), new Guid("1c83de98-bed7-460e-a6bc-cb8f98f570da"), new DateTimeOffset(new DateTime(2021, 10, 2, 18, 2, 4, 435, DateTimeKind.Unspecified).AddTicks(2259), new TimeSpan(0, 4, 0, 0, 0)), "Hotel 1 Review text 1", 1 },
                    { new Guid("deedb09c-34ef-42f1-b3d1-dec659cb2157"), new Guid("1c83de98-bed7-460e-a6bc-cb8f98f570da"), new DateTimeOffset(new DateTime(2021, 10, 1, 18, 2, 4, 439, DateTimeKind.Unspecified).AddTicks(6695), new TimeSpan(0, 4, 0, 0, 0)), "Hotel 1 Review text 2", 2 },
                    { new Guid("a687dc49-e155-427b-aba0-ca4193c68634"), new Guid("c29d678e-4985-468f-a525-ebb88fc150e0"), new DateTimeOffset(new DateTime(2021, 9, 30, 18, 2, 4, 439, DateTimeKind.Unspecified).AddTicks(6751), new TimeSpan(0, 4, 0, 0, 0)), "Hotel 2 Review text 1", 5 }
                });

            migrationBuilder.InsertData(
                table: "Hotels",
                columns: new[] { "HotelID", "Active", "Address", "CityID", "ClassRating", "Description", "ImageUrl", "IsPopular", "IsRecomended", "Location", "Name", "PricePerPaxPerNight" },
                values: new object[,]
                {
                    { new Guid("1c83de98-bed7-460e-a6bc-cb8f98f570da"), true, "Address of Hotel 1", new Guid("b81fb318-a4e3-4119-90d2-40ea62657df0"), 1, "Description of Hotel 1", "hotel_image.pic", null, null, "", "Hotel # 1", 101.00m },
                    { new Guid("c29d678e-4985-468f-a525-ebb88fc150e0"), true, "Address of Hotel 2", new Guid("b81fb318-a4e3-4119-90d2-40ea62657df0"), 2, "Description of Hotel 2", "hotel_image.pic", false, false, "", "Hotel # 2", 102.00m },
                    { new Guid("b30152f7-25b2-4ac1-9ccf-31d401d876f3"), true, "Address of Hotel 3", new Guid("202c5c6e-4cb6-4f4b-8c31-3e3ce69eb872"), 3, "Description of Hotel 3", "hotel_image.pic", true, false, "", "Hotel # 3", 103.00m },
                    { new Guid("2ad4c38b-bd8e-4d62-808c-bee7bad884d5"), true, "Address of Hotel 4", new Guid("202c5c6e-4cb6-4f4b-8c31-3e3ce69eb872"), 4, "Description of Hotel 4", "hotel_image.pic", true, true, "", "Hotel # 4", 104.00m },
                    { new Guid("2f3a1a09-69ee-488f-880e-fd1d38e28027"), true, "Address of Hotel 5", new Guid("7268be33-9c13-4773-b409-eb7d064291a8"), 5, "Description of Hotel 5", "hotel_image.pic", true, true, "", "Hotel # 5", 105.00m }
                });

            migrationBuilder.InsertData(
                table: "HotelFacilites",
                columns: new[] { "FacilitiesFacilityID", "HotelsHotelID" },
                values: new object[,]
                {
                    { new Guid("8db20b41-dbe3-44b9-b347-b28c990a56a3"), new Guid("1c83de98-bed7-460e-a6bc-cb8f98f570da") },
                    { new Guid("b3d1b6b1-999d-4157-bfcb-0e5f8fc45c9c"), new Guid("1c83de98-bed7-460e-a6bc-cb8f98f570da") },
                    { new Guid("5cc965c9-24ed-44bb-b11b-865fee07ca42"), new Guid("1c83de98-bed7-460e-a6bc-cb8f98f570da") },
                    { new Guid("8db20b41-dbe3-44b9-b347-b28c990a56a3"), new Guid("c29d678e-4985-468f-a525-ebb88fc150e0") },
                    { new Guid("b0bb0b79-1922-40eb-89a3-1c0e7737663b"), new Guid("c29d678e-4985-468f-a525-ebb88fc150e0") },
                    { new Guid("b0bb0b79-1922-40eb-89a3-1c0e7737663b"), new Guid("b30152f7-25b2-4ac1-9ccf-31d401d876f3") }
                });
        }
    }
}
