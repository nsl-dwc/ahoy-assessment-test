﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AHOY.DataAccessLibrary.Migrations
{
    public partial class PreRelease : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cities",
                columns: table => new
                {
                    CityID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cities", x => x.CityID);
                });

            migrationBuilder.CreateTable(
                name: "Facilities",
                columns: table => new
                {
                    FacilityID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    FacilityName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Facilities", x => x.FacilityID);
                });

            migrationBuilder.CreateTable(
                name: "Reviews",
                columns: table => new
                {
                    ReviewID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    HotelID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ReviewText = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UserRating = table.Column<int>(type: "int", nullable: true),
                    ReviewDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reviews", x => x.ReviewID);
                });

            migrationBuilder.CreateTable(
                name: "Hotels",
                columns: table => new
                {
                    HotelID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false),
                    CityID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ClassRating = table.Column<int>(type: "int", nullable: false),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Location = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PricePerPaxPerNight = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    IsRecomended = table.Column<bool>(type: "bit", nullable: true),
                    IsPopular = table.Column<bool>(type: "bit", nullable: true),
                    ImageUrl = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hotels", x => x.HotelID);
                    table.ForeignKey(
                        name: "FK_Hotels_Cities_CityID",
                        column: x => x.CityID,
                        principalTable: "Cities",
                        principalColumn: "CityID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Bookings",
                columns: table => new
                {
                    BookingID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    HotelID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CheckInDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    CheckOutDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    GuestQty = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bookings", x => x.BookingID);
                    table.ForeignKey(
                        name: "FK_Bookings_Hotels_HotelID",
                        column: x => x.HotelID,
                        principalTable: "Hotels",
                        principalColumn: "HotelID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HotelFacilites",
                columns: table => new
                {
                    FacilitiesFacilityID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    HotelsHotelID = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HotelFacilites", x => new { x.FacilitiesFacilityID, x.HotelsHotelID });
                    table.ForeignKey(
                        name: "FK_HotelFacilites_Facilities_FacilitiesFacilityID",
                        column: x => x.FacilitiesFacilityID,
                        principalTable: "Facilities",
                        principalColumn: "FacilityID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HotelFacilites_Hotels_HotelsHotelID",
                        column: x => x.HotelsHotelID,
                        principalTable: "Hotels",
                        principalColumn: "HotelID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Cities",
                columns: new[] { "CityID", "Name" },
                values: new object[,]
                {
                    { new Guid("b81fb318-a4e3-4119-90d2-40ea62657df0"), "City 1" },
                    { new Guid("202c5c6e-4cb6-4f4b-8c31-3e3ce69eb872"), "City 2" },
                    { new Guid("7268be33-9c13-4773-b409-eb7d064291a8"), "City 3" }
                });

            migrationBuilder.InsertData(
                table: "Facilities",
                columns: new[] { "FacilityID", "FacilityName" },
                values: new object[,]
                {
                    { new Guid("8db20b41-dbe3-44b9-b347-b28c990a56a3"), "Breakfast" },
                    { new Guid("b3d1b6b1-999d-4157-bfcb-0e5f8fc45c9c"), "WiFi" },
                    { new Guid("5cc965c9-24ed-44bb-b11b-865fee07ca42"), "Parking" },
                    { new Guid("b0bb0b79-1922-40eb-89a3-1c0e7737663b"), "SPA" }
                });

            migrationBuilder.InsertData(
                table: "Reviews",
                columns: new[] { "ReviewID", "HotelID", "ReviewDate", "ReviewText", "UserRating" },
                values: new object[,]
                {
                    { new Guid("5dcb3b09-ebd9-4f54-8f3a-5c323331061e"), new Guid("1c83de98-bed7-460e-a6bc-cb8f98f570da"), new DateTimeOffset(new DateTime(2021, 10, 2, 18, 2, 4, 435, DateTimeKind.Unspecified).AddTicks(2259), new TimeSpan(0, 4, 0, 0, 0)), "Hotel 1 Review text 1", 1 },
                    { new Guid("deedb09c-34ef-42f1-b3d1-dec659cb2157"), new Guid("1c83de98-bed7-460e-a6bc-cb8f98f570da"), new DateTimeOffset(new DateTime(2021, 10, 1, 18, 2, 4, 439, DateTimeKind.Unspecified).AddTicks(6695), new TimeSpan(0, 4, 0, 0, 0)), "Hotel 1 Review text 2", 2 },
                    { new Guid("a687dc49-e155-427b-aba0-ca4193c68634"), new Guid("c29d678e-4985-468f-a525-ebb88fc150e0"), new DateTimeOffset(new DateTime(2021, 9, 30, 18, 2, 4, 439, DateTimeKind.Unspecified).AddTicks(6751), new TimeSpan(0, 4, 0, 0, 0)), "Hotel 2 Review text 1", 5 }
                });

            migrationBuilder.InsertData(
                table: "Hotels",
                columns: new[] { "HotelID", "Active", "Address", "CityID", "ClassRating", "Description", "ImageUrl", "IsPopular", "IsRecomended", "Location", "Name", "PricePerPaxPerNight" },
                values: new object[,]
                {
                    { new Guid("1c83de98-bed7-460e-a6bc-cb8f98f570da"), true, "Address of Hotel 1", new Guid("b81fb318-a4e3-4119-90d2-40ea62657df0"), 1, "Description of Hotel 1", "hotel_image.pic", null, null, "", "Hotel # 1", 101.00m },
                    { new Guid("c29d678e-4985-468f-a525-ebb88fc150e0"), true, "Address of Hotel 2", new Guid("b81fb318-a4e3-4119-90d2-40ea62657df0"), 2, "Description of Hotel 2", "hotel_image.pic", false, false, "", "Hotel # 2", 102.00m },
                    { new Guid("b30152f7-25b2-4ac1-9ccf-31d401d876f3"), true, "Address of Hotel 3", new Guid("202c5c6e-4cb6-4f4b-8c31-3e3ce69eb872"), 3, "Description of Hotel 3", "hotel_image.pic", true, false, "", "Hotel # 3", 103.00m },
                    { new Guid("2ad4c38b-bd8e-4d62-808c-bee7bad884d5"), true, "Address of Hotel 4", new Guid("202c5c6e-4cb6-4f4b-8c31-3e3ce69eb872"), 4, "Description of Hotel 4", "hotel_image.pic", true, true, "", "Hotel # 4", 104.00m },
                    { new Guid("2f3a1a09-69ee-488f-880e-fd1d38e28027"), true, "Address of Hotel 5", new Guid("7268be33-9c13-4773-b409-eb7d064291a8"), 5, "Description of Hotel 5", "hotel_image.pic", true, true, "", "Hotel # 5", 105.00m }
                });

            migrationBuilder.InsertData(
                table: "HotelFacilites",
                columns: new[] { "FacilitiesFacilityID", "HotelsHotelID" },
                values: new object[,]
                {
                    { new Guid("8db20b41-dbe3-44b9-b347-b28c990a56a3"), new Guid("1c83de98-bed7-460e-a6bc-cb8f98f570da") },
                    { new Guid("b3d1b6b1-999d-4157-bfcb-0e5f8fc45c9c"), new Guid("1c83de98-bed7-460e-a6bc-cb8f98f570da") },
                    { new Guid("5cc965c9-24ed-44bb-b11b-865fee07ca42"), new Guid("1c83de98-bed7-460e-a6bc-cb8f98f570da") },
                    { new Guid("8db20b41-dbe3-44b9-b347-b28c990a56a3"), new Guid("c29d678e-4985-468f-a525-ebb88fc150e0") },
                    { new Guid("b0bb0b79-1922-40eb-89a3-1c0e7737663b"), new Guid("c29d678e-4985-468f-a525-ebb88fc150e0") },
                    { new Guid("b0bb0b79-1922-40eb-89a3-1c0e7737663b"), new Guid("b30152f7-25b2-4ac1-9ccf-31d401d876f3") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Bookings_HotelID",
                table: "Bookings",
                column: "HotelID");

            migrationBuilder.CreateIndex(
                name: "IX_HotelFacilites_HotelsHotelID",
                table: "HotelFacilites",
                column: "HotelsHotelID");

            migrationBuilder.CreateIndex(
                name: "IX_Hotels_CityID",
                table: "Hotels",
                column: "CityID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Bookings");

            migrationBuilder.DropTable(
                name: "HotelFacilites");

            migrationBuilder.DropTable(
                name: "Reviews");

            migrationBuilder.DropTable(
                name: "Facilities");

            migrationBuilder.DropTable(
                name: "Hotels");

            migrationBuilder.DropTable(
                name: "Cities");
        }
    }
}
