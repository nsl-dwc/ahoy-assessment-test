﻿using AHOY.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHOY.DataAccessLibrary.MsSqlServer
{
    public class ApplicationDbContext : DbContext//, IDBContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }

        public ApplicationDbContext(string connectionString)
        : this(new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseSqlServer(connectionString, ob => ob.EnableRetryOnFailure()).Options)
        {

        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Hotel>()
                .HasMany(h => h.Facilities)
                .WithMany(f => f.Hotels)
                .UsingEntity(x => x.ToTable("HotelFacilites"));
                

            modelBuilder.Seed();
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Hotel> Hotels { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Facility> Facilities { get; set; }
        public DbSet<Booking> Bookings { get ; set ; }
        public DbSet<City> Cities { get; set; }

    }
}
