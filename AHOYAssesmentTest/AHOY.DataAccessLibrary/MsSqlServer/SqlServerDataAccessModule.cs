﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Utils.Modules;

namespace AHOY.DataAccessLibrary.MsSqlServer
{
    public class SqlServerDataAccessModule: Module
    {
        public override void Load(IServiceCollection services)
        {
            services.AddDbContext<AHOY.DataAccessLibrary.MsSqlServer.ApplicationDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DatabaseConnection"), ob => ob.EnableRetryOnFailure()));           
        }
    }
}
