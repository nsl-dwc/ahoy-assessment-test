﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHOY.Entities
{
    public class HotelFacility
    {
        public Guid HotelFacilityID { get; set; }
        public Guid HotelID { get; set; }
        public Hotel Hotel { get; set; }

        public Guid FacilityID { get; set; }
        public Facility Facility { get; set; }
    }
}
