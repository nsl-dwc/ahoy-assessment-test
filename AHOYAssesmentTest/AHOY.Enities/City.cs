﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHOY.Entities
{
    public class City
    {
        [Key]
        public Guid CityID { get; set; }

        [Required]
        [MaxLength(150)]
        public string Name { get; set; }
    }
}
