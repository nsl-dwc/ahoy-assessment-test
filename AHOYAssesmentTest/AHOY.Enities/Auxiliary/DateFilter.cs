﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AHOY.Entities.Auxiliary
{
    public class DateFilter
    {
        public DateTimeOffset StartDate{ get; set; }
        public DateTimeOffset EndDate { get; set; }
    }
}
