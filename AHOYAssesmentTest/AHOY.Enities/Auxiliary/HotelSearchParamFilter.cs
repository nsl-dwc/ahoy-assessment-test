﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHOY.Entities.Auxiliary
{
    public class HotelSearchParamFilter
    {
        public string HoteName { get; set; }
        public string CityName { get; set; }
        public StarRating ClassRating { get; set; }
    }
}
