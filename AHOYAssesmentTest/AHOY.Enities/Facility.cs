﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AHOY.Entities
{
    public class Facility
    {
        [Key]
        public Guid FacilityID { get; set; }

        [Required]
        [MaxLength(50)]
        public string FacilityName { get; set; }

        //public ICollection<HotelFacility> HotelFacilities { get; set; } = new List<HotelFacility>();
        public ICollection<Hotel> Hotels{ get; set; } = new List<Hotel>();
    }
}