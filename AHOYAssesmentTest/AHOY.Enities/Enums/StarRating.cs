﻿namespace AHOY.Entities
{
    public enum StarRating
    {
        UnRated = 0,
        One = 1,
        Two =2,
        Three = 3,
        Four = 4,
        Five =5
    }
}