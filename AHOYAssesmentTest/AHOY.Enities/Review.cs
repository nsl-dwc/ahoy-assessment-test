﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHOY.Entities
{
    public class Review
    {
        [Key]
        public Guid ReviewID { get; set; }
        
        [Required]
        public Guid HotelID { get; set; }
        
        [Required]
        public string ReviewText { get; set; }
        public int? UserRating { get; set; }

        public DateTimeOffset ReviewDate { get; set; }

    }
}
