﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHOY.Entities
{
    public class Booking
    {
        [Key]
        public Guid BookingID { get; set; }
        
        [Required]
        public Guid HotelID { get; set; }
        
        [Required]
        public DateTimeOffset CheckInDate { get; set; }
        
        [Required]
        public DateTimeOffset CheckOutDate { get; set; }

        [Required]
        [Range(1, 5)]
        public int GuestQty { get; set; }
        [Column(TypeName = "decimal(10,2)")]
        public decimal PricePerPerson { get; set; }
        public decimal TotalCost { get => PricePerPerson * GuestQty; }

        [ForeignKey("HotelID")]
        public Hotel Hotel { get; set; }
    }
}
