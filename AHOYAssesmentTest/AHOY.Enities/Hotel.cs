﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHOY.Entities
{
    public class Hotel
    {
        [Key]
        public Guid HotelID { get; set; }
        
        [Required]
        [MaxLength(250)]
        public string Name { get; set; }

        [Required]
        public Guid CityID { get; set; }
        [ForeignKey("CityID")]
        public City City { get; set; }

        [Range(1,5)]
        public StarRating ClassRating { get; set; }        
        public string Address { get; set; }
        public string Location { get; set; } // string type instead of DbGeography is for simplicity
        public string Description { get; set; }        
        
        [Required]       
        [Column(TypeName ="decimal(15,2)")]
        public decimal PricePerPaxPerNight { get; set; }       
        public bool? IsRecomended { get; set; }
        public bool? IsPopular { get; set; } // may be calculated/rated based on bookings quantity
        
        [Required]
        public string ImageUrl { get; set; }

        //public ICollection<Review> Reviews { get; set; } = new List<Review>();
        //public double? AvgUserRating { get => Reviews.Average(s => s.UserRating); }
        
        public bool Active { get; set; } // Enable/Disabled for listing

        //public ICollection<HotelFacility> HotelFacilities { get; set; } = new List<HotelFacility>();
        public ICollection<Facility> Facilities { get; set; } = new List<Facility>();
    }
}
